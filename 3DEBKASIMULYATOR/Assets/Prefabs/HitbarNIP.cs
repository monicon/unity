﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitbarNIP : MonoBehaviour
{
    [SerializeField] Text _hp;
    // Start is called before the first frame update
    int _hitPoint;
    void Start()
    {
        _hitPoint = UnityEngine.Random.Range(50, 100);
    }

    // Update is called once per frame
    void Update()
    {
        _hp.text = _hitPoint.ToString();
    }
}
