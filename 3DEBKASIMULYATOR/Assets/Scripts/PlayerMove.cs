﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Transform cam;


    Vector3 Pleangles;
    Vector3 Camangles;
    Vector3 di;
    float sp = 10f;
    CharacterController сс;
    float gr = 1f;
    public Transform point;
    public GameObject pull;
    float cd = 0.5f;
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    private Vector3 moveDirection = Vector3.zero;
    public GameObject pule;

    void Start()
    {
        сс = GetComponent<CharacterController>();
        Cursor.visible = false;//для отключения курсора
        Cursor.lockState = CursorLockMode.Locked;//для блокировки курсора в центр
        Colorr();
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();

        Move();
    }
    void Colorr()
    {
        Color newColor = new Vector4(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
        gameObject.GetComponent<Renderer>().material.color = newColor;
        Color newColor1 = new Vector4(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
        transform.GetChild(0).GetComponent<Renderer>().material.color = newColor1;
    }
    private void Move()
    {
        float lastY = moveDirection.y;
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        moveDirection *= speed;
        moveDirection.y = lastY;

        if (сс.isGrounded)
        {
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        moveDirection.y -= gravity * Time.deltaTime;


        moveDirection = transform.TransformDirection(moveDirection);
        сс.Move(moveDirection * Time.deltaTime);

    }

    private void Rotate()
    {
        float q = Input.GetAxis("Mouse X");
        float w = Input.GetAxis("Mouse Y");

        Camangles.x -= w * Time.deltaTime * 300f;
        if (Camangles.x > 90) Camangles.x = 90;
        if (Camangles.x < -90) Camangles.x = -90;

        Pleangles.y += q * Time.deltaTime * 300f;

        transform.rotation = Quaternion.Euler(Pleangles);
        transform.GetChild(0).localRotation = Quaternion.Euler(Camangles);
    }
}
