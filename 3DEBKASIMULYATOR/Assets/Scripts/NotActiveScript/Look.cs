﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look : MonoBehaviour
{
    Vector3 Camangles;
    Vector3 Pleangles;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float q = Input.GetAxis("Mouse X");
        float w = Input.GetAxis("Mouse Y");
        Camangles.x -= w * Time.deltaTime * 300f;
        if (Camangles.x > 90) Camangles.x = 90;
        if (Camangles.x < -90) Camangles.x = -90;

        Pleangles.y += q * Time.deltaTime * 300f;

        transform.rotation = Quaternion.Euler(Pleangles);
        transform.GetChild(0).localRotation = Quaternion.Euler(Camangles);
    }
}
