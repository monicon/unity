﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove2 : MonoBehaviour
{
    public CharacterController CC;
    public Transform cam;
    public float speed = 10f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float hor = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(hor, 0f, vert).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targetAngles = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
 
            transform.rotation = Quaternion.Euler(0f, targetAngles, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngles, 0f) * Vector3.forward;
            CC.Move(moveDir.normalized * speed * Time.deltaTime);
        }
    }
}