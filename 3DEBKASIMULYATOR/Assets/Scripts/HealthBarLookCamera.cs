﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarLookCamera : MonoBehaviour
{
    public Transform cam;
    GameObject cameraG;
    // Start is called before the first frame update
    void Start()
    {
        cameraG = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(transform.position+cameraG.transform.forward);
    }
}
